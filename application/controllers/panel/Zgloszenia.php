<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Zgloszenia extends CI_Controller {

	public function index() {
		if(checkAccess($access_group = ['administrator', 'redaktor'], $_SESSION['rola'])) {
			if (!$this->db->table_exists($this->uri->segment(2))){
				$this->base_m->create_table($this->uri->segment(2));
			}
            // DEFAULT DATA
			$data = loadDefaultData();

			$data['rows'] = $this->back_m->get_all($this->uri->segment(2));
			echo loadSubViewsBack($this->uri->segment(2), 'index', $data);
		} else {
			redirect('panel');
		}
	}

	public function form($type, $id = '') {
		if(checkAccess($access_group = ['administrator', 'redaktor'], $_SESSION['rola'])) {
            // DEFAULT DATA
			$data = loadDefaultData();

            if($id != '') {
			    $data['value'] = $this->back_m->get_one($this->uri->segment(2), $id);
            }
			echo loadSubViewsBack($this->uri->segment(2), 'form', $data);
		} else {
			redirect('panel');
		}
	} 

	public function action($type, $table, $id = '') {
			
			$redirect_page = '';
			if($this->session->post('link') != ''){
				$this->form_validation->set_rules('link','Link','trim|valid_url');
				$this->form_validation->set_message('valid_url','Podany link jest nieprawidłowy!');
			}
			if($this->session->post('sitemap') != ''){
				if($this->session->post('sitemap') != 'sitemap.xml'){
					$this->session->set_flashdata('flashdata_error', 'Sitemapa powinna zawierać nazwę "sitemap.xml" !');
				}
			}
			$this->form_validation->set_rules('email','Email','trim|required|valid_email');
			
			$this->form_validation->set_message('valid_email','Podany email jest nieprawidłowy!');
			
			$now = date('Y-m-d');
			if (!is_dir('uploads/'.$now)) {
				mkdir('./uploads/' . $now, 0777, TRUE);
			}
			$config['upload_path'] = './uploads/'.$now;
			$config['allowed_types'] = 'xml';
			$config['max_size'] = 0;
			$config['max_width'] = 0;
			$config['max_height'] = 0;
			$this->load->library('upload',$config);
			$this->upload->initialize($config);
			
			foreach ($_POST as $key => $value) {

				if (!$this->db->field_exists($key, $table)) {
					$this->base_m->create_column($table, $key);
				}

				if($key == 'sitemap') {
					if ($this->upload->do_upload('sitemap')) {
						$data = $this->upload->data();
						$insert['sitemap'] = $now.'/'.$data['file_name'];  
						
						addMedia($data);
					} 
				} else {
					$insert[$key] = $value; 
				}
            }
            if($this->input->post('rodo1') != null ) { 
                $insert['rodo1'] = 'Zaakceptowane';
            } else {
                $insert['rodo1'] = 'Niezaakceptowane';
            }
            if($this->input->post('rodo2') != null ) { 
                $insert['rodo2'] = 'Zaakceptowane';
            } else {
                $insert['rodo2'] = 'Niezaakceptowane';
            }
            if($this->form_validation->run() == FALSE){
            	// $this->back_m->insert($table, $insert);
			    $this->session->set_flashdata('flashdata_error', validation_errors());
			    redirect('home/index');
            }else{
            	if($type == 'insert'){
            		$this->back_m->insert('zgloszenia', $insert);
			    	$this->session->set_flashdata('flashdata_success', 'Rekord został dodanyyy!');
			    	redirect('panel/dashboard');
            	}else{
					$this->back_m->update($table, $insert, $id);
					$this->session->set_flashdata('flashdata_success', 'Rekord został zaktualizowany!');
					redirect('panel/zgloszenia');
            	}
            	
            }
            
			
		
    }
}